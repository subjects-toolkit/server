import requests
import json


def find_courses(fac_id=None) -> [str]:
    courses_details = []
    URL = 'https://apps.usos.pw.edu.pl/services/courses/search?num=20'
    parameters = {"num": 20, "start": 0}
    if fac_id is not None:
        parameters["fac_id"] = fac_id
    records_per_page = 20
    is_next_page = True
    while is_next_page:
        response = requests.get(URL, params=parameters)
        parameters["start"] = parameters["start"] + records_per_page
        jsonObject = response.json()
        is_next_page = jsonObject["next_page"]
        items = jsonObject["items"]

        for item in items:
            course_id = item["course_id"]
            courses_details.append(course_details(course_id))

    return courses_details


def course_details(course_id: str):
    URL = 'https://apps.usos.pw.edu.pl/services/courses/course'
    parameters = {
        "course_id": course_id,
        "fields": "id|name|profile_url|attributes2|ects_credits_simplified"
    }
    response = requests.get(URL, params=parameters)
    jsonObject = response.json()
    if jsonObject["name"]["pl"] == '':
        course_name = jsonObject["name"]["en"]
    else:
        course_name = jsonObject["name"]["pl"]
    details = {
        "id": jsonObject["id"],
        "name": course_name,
        "profile_url": jsonObject["profile_url"],
        "shortcut": "",
        "ECTS": jsonObject["ects_credits_simplified"]
    }
    for atribute in jsonObject["attributes2"]:
        if atribute["name"]["pl"] == "Kod wydziałowy":
            details["shortcut"] = atribute["values"][0]["label"]["pl"]
    return details


def add_courses_to_database(URL: str, courses):
    correct_requests = 0
    for course in courses:
        post_query = {"name": course["name"], "shortcut": course["shortcut"]}
        request = requests.post(URL + "/subjects/", json=post_query)
        if request.status_code == 201:
            correct_requests += 1
    return correct_requests


URL = input("podaj URL aplikacji\n")
courses = find_courses(fac_id=103000)
correct_requests = add_courses_to_database(URL, courses)

print("Znalezionych przedmiotów:")
print(len(courses))
print("Wstawionych do bazy:")
print(correct_requests)