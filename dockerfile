FROM registry.gitlab.com/subjects-toolkit/server/server_base:latest

ENV MICRO_SERVICE=/home/app/server
ENV APP_USER=user
RUN addgroup -S $APP_USER && adduser -S $APP_USER -g $APP_USER
# set work directory

RUN mkdir -p $MICRO_SERVICE
RUN mkdir -p $MICRO_SERVICE/server_app/static

# where the code lives
WORKDIR $MICRO_SERVICE

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip
# copy project
COPY . $MICRO_SERVICE
RUN pip install -r requirements.txt
COPY ./entrypoint.sh $MICRO_SERVICE

CMD ["/bin/bash", "/home/app/server/entrypoint.sh"]
