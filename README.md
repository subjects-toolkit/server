testing

# server

## Pobieranie

```bash
git clone git@gitlab.com:subjects-toolkit/server.git
cd server
git submodule update --init

```

## Używane narzędzia
Wszystkie potrzebne pakiety znajdują się w pliku requirements.txt

### Autoformatowanie kodu
Autoformatowania kodu można dokonać przy pomocy polecenia:
```
yapf -i -r ./
```

### Pokrycie kodu testami
Sprawdzenie pokrycia kodu można wykonać używając poleceń:
```
pytest --cov=server/ manage.py
coverage xml
```
## Uruchamianie modułu Django
Niezależnie od posiadanego IDE w tym momencie, przy każdym projekcie warto mieć wirtualne środowisko, w którym instalujemy potrzebne pakiety.
Należy posiadać Django. W tym celu wykonujemy komendy _[później zaimplementujemy skrypty do deploymentu i requirements.txt]_
_W Ubuntu 20.04 może wystarczyć napisanie python zamiast python3, ale na starszych wersjach konieczne jest użycie python3_

```bash
python3 -m pip install django
```
Aby uruchomić serwer należy wpisać, będąc w głównym folderze projektu:
```bash
python3 manage.py runserver
```
Pracę serwera przerywamy CTRL+C

## Testowanie
Testy uruchamiamy komendą
```bash
python3 manage.py test
```

## Dockeryzacja
Należy zainstalować Dockera, instrukcje do instalacji znajdują się tu: 
Z Dockerem uruchamiamy aplikację w następujący sposób:
1) Wchodzimy do folderu głównego
2) Wpisujemy
```bash
docker-compose up --build --renew-anon-volumes
```
3) Ostatnim komunikatem w konsoli powinno być "Booting worker with pid: X", gdzie X to jakaś liczba. Po tym komunikacie serwer jest gotowy, wystarczy wejść na 0.0.0.0:1300 żeby uruchomić stronę startową. Pod 0.0.0.0:1300/meme przykład plików statycznych.

Jeśli chcemy uruchomić Dockera w tle, używamy komendy:
```bash
docker-compose up --build --renew-anon-volumes -d
```
Wówczas zatrzymuje się go
```bash
docker-compose stop
```
## Bez Dockera
Po wprowadzonych zmianach jeśli chcemy w ogóle pominąć Dockera i używać komend w stylu "python3 manage.py test" lub "python3 manage.py runserver" to musimy wprowadzić dwie komendy (odpowiednio zainicjalizować bazę danych i zmienić hosta tejże bazy)
```bash
sudo -u postgres psql -f init.sql
export DB_HOST=localhost
```
Potem nie trzeba nic cofać, wystarczy używać komendy z działu Dockeryzacja

## Model bazy danych
### Migracja modelu
Aby zmigrować model do projektu należy wywołać polecenia:
```bash
python3 manage.py makemigrations server_app
python3 manage.py migrate
```
Po wywołaniu pierwszego polecenia pojawi się komunikat typu:
```
Migrations for 'server_app':
  server_app/migrations/0002_auto_20210407_1443.py
    - Create model Lecturer
    - Create model Opinion
```
W tym przypadku aby uzyskać kod SQL bazy danych należy wywołać polecenie:
```bash
python3 manage.py sqlmigrate server_app 0002
```
Podmieniając `0002` na numer który pojawił się w poprzednim komunikacie 
### Wizualizacja modelu
Wymagane jest zainstalowanie pakietu pygraphviz wywołując dwa polecenia
```bash
sudo apt install python3-pygraphviz
pip3 install django-extensions pygraphviz
```
Wizualizację modelu bazy danych można utworzyć i zapisać do pliku o nazwie plik.png wywołując z poziomu folderu głównego polecenie:
```bash
python3 manage.py graph_models -g -n server_app -o plik.png
```
# Diagram architektury aplikacji
![](docs/architecture_diagram.png)

## REST API
Dostępne jest pobieranie oraz wstawianie przedmiotów przez REST API.

Można pobrać listę wszystkich przedmiotów w bazie przy pomocy URL:
```
<IP>:<nr portu>/subjects
```
Można też pobieraż przedmioty pojedynczo:
```
<IP>:<nr portu>/subjects/<ID przedmiotu>
```

## Aplikacja USOSowa
Uruchomienie aplikacji:
```
python3 usos_app/usos_app.py
```
następnie należy podać adres URL serwera wraz z portem

Aplikacja wyszuka przy pomocy API USOSa przedmioty i doda je do bazy danych

