FROM python:3.8.3-alpine

# install psycopg2 dependencies
RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev \
    && apk add postgresql-dev gcc python3-dev musl-dev \
    && apk del build-deps \
    && apk --no-cache add musl-dev linux-headers g++
# install dependencies
RUN pip install --upgrade pip

COPY requirements.txt /opt/app/requirements.txt
RUN pip install -r /opt/app/requirements.txt